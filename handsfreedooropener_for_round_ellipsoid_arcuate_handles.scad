
$fn = 100;

// Width of the hands-free-door-opener, 50 - 60 mm seems to be a good size
width = 55;

// Thickness of the hands-free-door-opener, 3-5 mm seems to be a good value
wall_thickness = 3.5;

// smallest width of the enforcement
enforcement_thickness = 1.5;

// length of the arm plate of your hands-free-door-opener. Around 90mm seems to be a good value
length_armplate = 90;

// Ahhrrr, don't know how to explain this. You will figure it out :/
r_arm = 30;
phi_arm = 60;

// width of the split, e.g. the distance between the two parts
split_size = 1.5;

// Diameter of the screwholes. e.g. for M4 screws chose a diameter slight above 4mm
screw_hole_diameter = 4.5;

// Diameter/length of your door handle
// If your handle is ellipsoid: measure both length, l1 > l2
// If you have round handle, both values should be the same
// And you probably want to add 0.5-1mm, if you want to add some rubber tape
ellipse_l1 = 20;
ellipse_l2 = 20;

// If you door handle is arcuate, then measure these values
// If your door handle is straight, then set a = 0
s = 100;        // length of chord (Länge der Kreissehne)
a = 0.0;          // hight of the arc (Hoehe des Kreissegmentes=


// if your handle is ellipsoid, you want to specify this angle, maybe somewhere between 20 and 40 degrees
// If you handle ist round, this value does not matter, since you can rotate the door-opener. In this case, just set alpha = 0;
alpha = 0;





r = (4*pow(max(a,0.001),2) + pow(max(s,0.001),2))/(8*max(a,0.001));



module arc_arm(){
    translate([-r_arm,width,length_armplate-r_arm]) 
    rotate([90,0,0])
    linear_extrude(height=width){
        intersection(){
            difference(){
                circle(r=r_arm+wall_thickness);
                circle(r=r_arm);
            }
            hull(){
                square([1,1]);
                translate(2*(r_arm+wall_thickness)*[cos(0),sin(0)]) square([1,1]);
                translate(2*(r_arm+wall_thickness)*[cos(phi_arm),sin(phi_arm)]) square([1,1]);
            }
        }
        translate((r_arm+wall_thickness/2)*[cos(0),sin(0)]) circle(r=wall_thickness/2);
        translate((r_arm+wall_thickness/2)*[cos(phi_arm),sin(phi_arm)]) circle(r=wall_thickness/2);
    }
}

module handle_plate(){
    cube([wall_thickness,width,length_armplate-r_arm]);
    arc_arm();
    hull(){
        translate([2*wall_thickness,0,wall_thickness+2*wall_thickness*sin(alpha)])
            rotate([-90,0,0]) cylinder(h=wall_thickness/2,r=wall_thickness);
        translate([0,0,length_armplate-r_arm-1]) cube([wall_thickness,wall_thickness/2,1]);
        cube([wall_thickness,wall_thickness/2,1]);
        translate([0,2*wall_thickness,wall_thickness]) cube([wall_thickness,wall_thickness,1]);
    }
    hull(){
       translate([2*wall_thickness,width-wall_thickness/2,wall_thickness+2*wall_thickness*sin(alpha)])
            rotate([-90,0,0]) cylinder(h=wall_thickness/2,r=wall_thickness);
       translate([0,width-wall_thickness/2,length_armplate-r_arm-1]) cube([wall_thickness,wall_thickness/2,1]);
       translate([0,width-wall_thickness/2,0]) cube([wall_thickness,wall_thickness/2,1]);
       translate([0,width-3*wall_thickness,wall_thickness]) cube([wall_thickness,wall_thickness,1]);
    }
}



module inner_tube(){
    translate([-r-ellipse_l2/2,0]) 
    intersection(){
        rotate_extrude(angle = 360) 
            translate([ellipse_l2/2+r,0])
                rotate([0,0,90])
                    scale([ellipse_l1/ellipse_l2,1]) circle(r=ellipse_l2/2);
        translate([0,-width/2-1,-ellipse_l1/2]) cube([r+ellipse_l2,width+2,ellipse_l1]);       
       // make inner tube a bit longer for proper difference() function 
    }
}

module outer_tube(){
    translate([-r-ellipse_l2/2,0]) 
    intersection(){
        rotate_extrude(angle = 360) 
            translate([ellipse_l2/2+r,0])
                rotate([0,0,90])
                    scale([(ellipse_l1+2*wall_thickness)/ellipse_l1,(ellipse_l2+2*wall_thickness)/ellipse_l2])
                        scale([ellipse_l1/ellipse_l2,1]) circle(r=ellipse_l2/2);
        translate([0,-width/2,-ellipse_l1/2-wall_thickness]) 
            cube([r+ellipse_l2+wall_thickness,width,ellipse_l1+2*wall_thickness]);       
    }
}

module handle_tube(){
    difference(){
        outer_tube();
        inner_tube();
    }
}

module handle_tube_innerhalf(){
    difference(){
        handle_tube();
        translate([ellipse_l2/2+wall_thickness-split_size/2,0]) 
            cube([ellipse_l2+2*wall_thickness,width+2,ellipse_l1+2*wall_thickness],center=true);
    }
}

module handle_tube_outerhalf(){
    intersection(){
        handle_tube();
        translate([ellipse_l2/2+wall_thickness+split_size/2,0]) 
            cube([ellipse_l2+2*wall_thickness,width-6*wall_thickness,ellipse_l1+2*wall_thickness],center=true);
    }
}




module outer_handle_noholes(){
    handle_tube_outerhalf();
    translate([split_size/2,0,-ellipse_l1/2-wall_thickness])
        rotate([0,alpha,0])
            translate([0,-width/2+3*wall_thickness,-3*screw_hole_diameter-split_size*sin(alpha)]) 
    cube([wall_thickness,width-6*wall_thickness,3*screw_hole_diameter+split_size*sin(alpha)]);
    hull(){
        translate([split_size/2,0,-ellipse_l1/2-wall_thickness]) rotate([0,alpha,0])
            translate([0,-width/2+3*wall_thickness,-0.1])
                cube([wall_thickness,width-6*wall_thickness,0.1]);
        translate([split_size/2,-width/2+3*wall_thickness,-ellipse_l1/2])
            cube([wall_thickness,width-6*wall_thickness,0.1]);
    }
    translate([split_size/2,-width/2+3*wall_thickness,ellipse_l1/2+wall_thickness]) rotate([0,alpha,0])
        cube([wall_thickness,width-6*wall_thickness,1.5*screw_hole_diameter+max(screw_hole_diameter,wall_thickness)+wall_thickness]);
    hull(){
        translate([split_size/2,-width/2+3*wall_thickness,ellipse_l1/2+wall_thickness]) rotate([0,alpha,0])
            cube([wall_thickness,width-6*wall_thickness,0.1]);
        translate([split_size/2,-width/2+3*wall_thickness,ellipse_l1/2]) 
            cube([wall_thickness,width-6*wall_thickness,0.1]);
    }
}

module inner_handle_noholes(){
    handle_tube_innerhalf();
    translate([-split_size/2,-width/2,ellipse_l1/2]) 
        rotate([0,alpha,0]) translate([-wall_thickness,0,0]) handle_plate();
    translate([-split_size/2,0,-ellipse_l1/2-wall_thickness]) rotate([0,alpha,0])
        translate([-wall_thickness,-width/2,-3*screw_hole_diameter])
            cube([wall_thickness,width,3*screw_hole_diameter]);
    hull(){
        translate([-split_size/2,0,-ellipse_l1/2-wall_thickness]) rotate([0,alpha,0])
            translate([-wall_thickness,-width/2,-0.1])
                cube([wall_thickness,width,0.1]);
        translate([-split_size/2-wall_thickness,-width/2,-ellipse_l1/2-wall_thickness])
            cube([wall_thickness,width,wall_thickness/2]);
    }
}


module inner_handle(){
    difference(){
        inner_handle_noholes();
        translate([0,-width/2+3*wall_thickness+1.5*screw_hole_diameter,-ellipse_l1/2-wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,-1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
        translate([0,width/2-3*wall_thickness-1.5*screw_hole_diameter,-ellipse_l1/2-wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,-1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
        translate([0,-width/2+3*wall_thickness+1.5*screw_hole_diameter,ellipse_l1/2+wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
        translate([0,width/2-3*wall_thickness-1.5*screw_hole_diameter,ellipse_l1/2+wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
    }
}

module outer_handle(){
    difference(){
        outer_handle_noholes();
        translate([0,-width/2+3*wall_thickness+1.5*screw_hole_diameter,-ellipse_l1/2-wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,-1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
        translate([0,width/2-3*wall_thickness-1.5*screw_hole_diameter,-ellipse_l1/2-wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,-1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
        translate([0,-width/2+3*wall_thickness+1.5*screw_hole_diameter,ellipse_l1/2+wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
        translate([0,width/2-3*wall_thickness-1.5*screw_hole_diameter,ellipse_l1/2+wall_thickness]) rotate([0,alpha,0]) 
            translate([0,0,1.5*screw_hole_diameter]) rotate([0,90,0]) 
                cylinder(r=screw_hole_diameter/2,h=4*wall_thickness,center=true);
    }
}

translate([0,0,width/2])
rotate([90,0,90]) rotate([0,-alpha,0]) 
inner_handle();

translate([0,3*wall_thickness,width/2-3*wall_thickness]) rotate([90,0,90]) rotate([0,-alpha,0]) 
outer_handle();